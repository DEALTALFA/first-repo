# This file is a template, and might need editing before it works on your project.
FROM python:3.6
LABEL   description="Using tensorflow base image build a Xray model that predict wether the person iS COVID Positive or Negative \
        Python Packages= flask and pillow"
# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/* && apt autoremove \
    && /usr/local/bin/python -m pip install --upgrade pip && pip install flask pillow

WORKDIR /usr/src/app

COPY . /usr/src/app

EXPOSE 80

ENTRYPOINT ["python3"]
CMD ["-m","flask","run","--host=0.0.0.0","--port=80"]
